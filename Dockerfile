FROM alpine:3.12
ENV VERSION=2.3.2

RUN apk add --update nodejs-lts npm 

RUN npm install -g mountebank@${VERSION} --production

USER guest

EXPOSE 2525
EXPOSE 8000-8888
ENTRYPOINT ["mb"]
CMD ["start", "--pidfile", "/tmp/mb.pid", "--logfile", "/tmp/mb.log"]
