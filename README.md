# Description
Docker image for the great [mountebank](http://www.mbtest.org/) test tool. 

# Usage
Fetch it using podman/docker:

```
> podman run -p 2525:2525 registry.gitlab.com/soerenschneider/mountebank-docker
Trying to pull registry.gitlab.com/soerenschneider/mountebank-docker...Getting image source signatures
Copying blob 050382585609 skipped: already exists
Copying blob 601bced58495 done
Copying blob 8a2ec5fee599 done
Copying config 3d2c549a27 done
Writing manifest to image destination
Storing signatures
info: [mb:2525] mountebank v2.1.0 now taking orders - point your browser to http://localhost:2525/ for help
^Cinfo: [mb:2525] Adios - see you soon?
```

The docker image tag will follow the mountebank git tags. 
